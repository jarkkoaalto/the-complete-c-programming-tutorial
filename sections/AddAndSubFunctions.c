/*
 ============================================================================
 Name        : AddAndSubFunctions.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Add() and sub() Functions in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>


int main() {
	int num1, num2, substract, sum;
	printf("Enter the two values :\n");
	scanf("%d %d", &num1, &num2);
	sum = add(num1, num2);
	substract = sub(num1, num2);
	printf("Sum is: %d \nand \nSunstract is %d",sum, substract);
}

int add(int a, int b){
	int add;
	add = a+b;
	return add;
}

int sub(int a, int b){
	int result;
	result = a-b;
	return result;
}
