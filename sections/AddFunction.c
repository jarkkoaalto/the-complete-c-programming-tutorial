/*
 ============================================================================
 Name        : AddFunction.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Add() Function in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>


int main() {
	int num1, num2, sum =0;
	printf("Enter the two values :\n");
	scanf("%d %d", &num1, &num2);
	sum = add(num1, num2);
	printf("Sum is: %d",sum);
}

int add(int a, int b){
	int add;
	add = a+b;
	return add;
}
