/*
 ============================================================================
 Name        : AddTwoDesimalNumbers.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Add two decimal numbers in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(){

	float num1;
	float num2;
	float total;
	printf("Enter num1 and num2 separed by empy line :");
	scanf("%f %f",&num1, &num2);


	total = num1 + num2;
	printf("Total of two number is %f", total);
	return 0;

}
