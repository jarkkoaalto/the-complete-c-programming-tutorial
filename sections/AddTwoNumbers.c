/*
 ============================================================================
 Name        : AddTwoNumbers.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Add two numbers in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(){

	int num1;
	int num2;
	int total;
	printf("Enter num1 :");
	scanf("%d",&num1);
	printf("Enter num2 :");
	scanf("%d", &num2);

	total = num1 + num2;
	printf("Total of two number is %d", total);
	return 0;

}
