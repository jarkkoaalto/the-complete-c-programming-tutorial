/*
 ============================================================================
 Name        : ArraysIntroduction.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Arrays Introduction in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * Arrays are of two types
 * one- and multidimensional arrays
 */

int main() {
	int age[5];
	age[0] = 3;
	age[1] = 12;
	age[4] = 33;

	for(int i=0; i<5;i++){
		printf("%d\n",age[i]);
	}

	int i;
	for(i=0;i<5;i++){
		printf("Value in a [%d] is %d\n",i,age[i]);

	}

	/*
	int num[5];
	int a ;

	printf("Enter (int) number\n");
	scanf("%d", &a);
		a = num[0];
	printf("Enter (int) number\n");
	scanf("%d", &a);
		a = num[1];
	printf("Enter (int) number\n");
	scanf("%d", &a);
		a = num[2];
	printf("Enter (int) number\n");
	scanf("%d", &a);
		a = num[3];
	printf("Enter (int) number\n");
	scanf("%d", &a);
		a = num[4];

	for(int i=0;i<5;i++){
		printf("%d\n",num[i]);
	}
	*/
	return 0;
}


