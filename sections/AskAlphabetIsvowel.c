/*
 ============================================================================
 Name        : AskAlphabetIsvowel.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description :	Ask Alphabet Is that alphabet a vowel in C, Ansi-style
 ============================================================================
 */
#include <stdio.h>
#include <string.h>


int main() {

	char c;
	printf("Enter an alphabet");
	scanf("%c", &c);
	if(c=='a' || c=='e' || c=='i' || c=='o'||c=='u')
		printf("%c is a vowel",c);
	else{
		printf("\n%c is a consonant",c);
	}
	return 0;
}
