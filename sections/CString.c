/*
 ============================================================================
 Name        : CString.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Intorduction C String in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	// Assigning string to array
	char c[5] = "peter";
	for(int i=0;i<5;i++){
		printf("%c",c[i]); // c[0]=p, c[1]=e,c[2]=t,c[3]=e,c[4]=r => peter
	}
	return 0;
}
