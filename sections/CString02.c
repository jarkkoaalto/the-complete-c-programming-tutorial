/*
 ============================================================================
 Name        : CString02.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Intorduction C String in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	char c[10] = "peter john";
	printf("name is:%s", c);
	return 0;
}
