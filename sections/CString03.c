/*
 ============================================================================
 Name        : CString03.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Intorduction C String in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	char c[20];
	printf("Enter the name: ");
	//scanf("%s",c); // if you type e.g. harry potter only harry printed.
	gets(c); // printed value proper
	// printf("name is:%s", c);
	puts(c);
	return 0;
}
