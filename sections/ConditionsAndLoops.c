/*
 ============================================================================
 Name        : ConditionsAndLoops.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Conditions and Loops in C, Ansi-style
 ============================================================================
 */
#include <stdio.h>
#include <string.h>


int main() {
	int number;
	printf("Enter the number: ");
	scanf("%d", &number);

	if(number <0){
		printf("\nThe number is negative");
	}
	else {
		printf("\nThe number that your entered is positive number");
	}

	return 0;
}
