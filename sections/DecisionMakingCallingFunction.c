/*
 ============================================================================
 Name        : DecisionMakingCallingFunction.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Decision Making Calling a Function.c in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>


int main() {
	int num1, num2, substract, sum, a;
	printf("Enter 1 for addition, Enter 2 for substraction\n");
	scanf("%d", &a);
	printf("Enter the two  values\n ");
	scanf("%d %d", &num1, &num2);

	if(a == 1){
	sum = add(num1, num2);
	printf("Sum is: %d \n",sum);
	}
	else if(a==2){
	substract = sub(num1, num2);
	printf("Sunstract is %d", substract);
	}
	else {
		printf("Invalid value");
	}

	return 0;
}

int add(int a, int b){
	int add;
	add = a+b;
	return add;
}

int sub(int a, int b){
	int result;
	result = a-b;
	return result;
}
