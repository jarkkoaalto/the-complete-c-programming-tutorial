/*
 ============================================================================
 Name        : DifferentLoops.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description :Different Loops in C, Ansi-style
 ============================================================================
 */
#include <stdio.h>
#include <string.h>


int main() {

	int s;
	for(int i=0;i<10;i++){
		printf("Hi there %d ", i);
	}

	while(s < 10){
		printf("Moi %d ",s);
		s++;
	}

	s = 0;
	do{
		printf("Hi %d ",s);
		s++;
	}while(s < 10);

	return 0;
}
