/*
 ============================================================================
 Name        : DisplayUserInput.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Display user input.c in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(){

	char firstname[10];
	char lastname[10];
	printf("Enter the first name:\n");
	scanf("%s", firstname);
	printf("Enter the last name:\n");
	scanf("%s", lastname);

	printf("The name of the person is %s %s", firstname, lastname);
	return 0;

}
