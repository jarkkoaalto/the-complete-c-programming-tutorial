/*
 ============================================================================
 Name        : DynamicMemoyAllocation.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Dynamic Memory Allocation in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
 * malloc() allocate more memory
 * calloc() allocate less memory
 * free() free the memory allocation
 * realloc() reallocate memory
 * need #include <stdlib.h>
 */

int main() {
		char *a;
		a = malloc(20*sizeof(char));
		if(a == NULL)
		{
			printf("Couldn't able to allocate requested memory\n");
		}
		else
		{
			strcpy(a, "Dynamic memory allocation");
		}
		printf("Dynamically allocated memory content: %s\n", a );
		free(a);


		a = calloc(20, sizeof(char));
		if(a == NULL)
		{
			printf("Couldn't able to allocate requested memory\n");
		}
		else
		{
			strcpy(a, "Duynamic memory allocation");
		}
		printf("Dynamically allocated memory content: %s\n", a );
		free(a);


		a = realloc(a,100*sizeof(char));
		if(a == NULL)
		{
			printf("Couldn't able to allocate requested memory\n");
		}
		else
		{
			strcpy(a, "Duynamic memory allocation");
		}
		printf("Dynamically allocated memory content: %s\n", a );
		free(a);
	return 0;
}

