/*
 ============================================================================
 Name        : FunctionCall.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : call function in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

void Display(char ch[]); // function prototype


int main(void) {
	char c[50];
	printf("Enter string:\t");
	gets(c);
	Display(c);
	return 0;
}

void Display(char ch[]){
	printf("String output:\t");
	puts(ch);
}


