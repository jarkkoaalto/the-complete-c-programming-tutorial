/*
 ============================================================================
 Name        : LargestNumber.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description :Largest number in C, Ansi-style
 ============================================================================
 */
#include <stdio.h>
#include <string.h>


int main() {

	int a, b, c;
	printf("Enter three numbers separated by tab: ");
	scanf("%d %d %d", &a,&b,&c);

	if(a>b && a>c){
		printf("The largest number is %d", a);
	}
	if(b>a && b>c){
		printf("The largest number is %d", b);
	}
	if(c>a && c>b){
		printf("The largest number is %d", c);
	}

	return 0;
}
