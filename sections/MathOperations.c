/*
 ============================================================================
 Name        : MathOperations.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Math operations in C,

- sqrt
- area
-
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main(){

	int ans, num;
	float r, area;


	printf("Enter the number : \n");
	scanf("%d", &num);
	ans = sqrt(num);
	printf("Square root of the input number is %d",ans);


	printf("\nEnter the radius: ");
	scanf("%f",&r);

	area = 3.1415 * r * r;
	printf("\nArea of the circle is %f", area);
	return 0;


	return 0;
}
