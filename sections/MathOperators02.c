/*
 ============================================================================
 Name        : hrllo.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */
#include <stdio.h>
#include <string.h>


int main() {
	int dividend, divisor, quotient, remainder;
	printf("Enter Divided : ");
	scanf("%d", &dividend);
	printf("Enter divisor: ");
	scanf("%d", &divisor);

	quotient = dividend/divisor;
	remainder = dividend%divisor;
	printf("Quotient  = %d\nRemainder = %d",quotient, remainder);

	return 0;
}
