/*
 ============================================================================
 Name        : MultiplicationTable.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description :	Multiplication table in C, Ansi-style
 ============================================================================
 */
#include <stdio.h>
#include <string.h>


int main() {

	int a, i;
	printf("Enter the random number:");
	scanf("%d",&a);
	for(i=1;i<10;i++){
		printf("\n%d * %d = %d",a,i,(a*i));
	}
	return 0;
}
