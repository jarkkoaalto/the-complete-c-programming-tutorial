/*
 ============================================================================
 Name        : Pointers.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : poinbters in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
/*
 * pointers
 * int a = memori location add 2003054
 * int b = memori location add 3404005
 * pointer point this address
 *
 * int *a is pointer
 * int *a = &b;
 */
int main(void) {



	int a = 55;
	int *b = &a;
	printf("value of pointer b = %d\n", &a);
	printf("value of a = %d\n",a);
	printf("memory address of a = %d\n", &a);
	return 0;
}
