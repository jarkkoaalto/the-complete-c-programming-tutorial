/*
 ============================================================================
 Name        : Pointers02.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : poinbters in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
/*
 * pointers
 * int a = memori location add 2003054
 * int b = memori location add 3404005
 * pointer point this address
 *
 * int *a is pointer
 * int *a = &b;
 */
int main() {
	int *pc,c;
	c=22;
	printf("Address of c: %i \n",&c);
	printf("Value of c: %d \n",c);
	pc=&c;
	printf("Address of pointer pc: %i \n",pc);
	printf("Content of pointer pc: %d \n",*pc);
	c=11;
	printf("Address of pointer pc: %i \n",pc);
	printf("Content of pointer pc: %d \n",*pc);
	*pc=2;
	printf("Address of c:%i \n",&c);
	printf("Value of c:%d \n",c);

	return 0;
}
