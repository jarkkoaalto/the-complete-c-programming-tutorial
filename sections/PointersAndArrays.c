/*
 ============================================================================
 Name        : PointersAndArrays.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Pointers And Arrays in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main() {

	// one dimensional array
	int a[5] = {1,2,3,4,5};
	int *p;
	// p = &a[0];
	// or
	 p = a;
	printf("%d \n",p);  // pointing address

	// multi dimensional array
	int b[2][2];
	*(*(b+1)+1) = a[1][1];

	// int b[i][j];
	//*(*(a+i)+j) = a[i][j];
	return 0;
}
