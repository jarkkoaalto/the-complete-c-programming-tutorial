/*
 ============================================================================
 Name        : PointersAndArrays02.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Pointers And Arrays in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
	int i;
	int a[5] = {1,2,3,4,5};
	int *p;

	p = a;

	for(i=0;i<5;i++){
		printf("%d\n",*(a+i));
	}

	for(i=0;i<5;i++){
		printf("%d\n", *p);
		p++; // need incrementing, without p++ print 1 1 1 1 1
	}

	return 0;
}
