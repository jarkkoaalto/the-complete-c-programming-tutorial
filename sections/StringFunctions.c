/*
 ============================================================================
 Name        : StringFunctions.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : String functions in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
 * strcat()
 * strncat()
 * strcpy()
 * strlen()
 * strlwr()
 * strupr()
 * strrev()
 */
int main(void) {
	char str1[12] = "Hello";
    char str2[12] = "World";
    char str3[12];
    int len;

    strcpy(str3, str1); // copy
    printf("strcpy (str3,str1) : %s\n",str3);

    strcat(str1, str2);
    printf("dtrcat (str1, str2) : %s\n",str1);
    len = strlen(str1);
    printf("strlen(str1) : %d\n", len);

	return 0;
}
	/*
     * strcpy (str3,str1) : Hello
     * dtrcat (str1, str2) : HelloWorld
     * strlen(str1) : 10
     */


