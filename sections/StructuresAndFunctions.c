/*
 ============================================================================
 Name        : StructuresAndFunctions.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Structures And Functions in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

struct student{
	char name[70];
	int roll;
};

void Display(struct student stu);
// function prototype should be below is the structure declaration
// compile shows error.

int main() {
	struct student s1;
	printf("Enter strudent's name:\n");
	scanf("%s", &s1.name);
	printf("Enter roll number:\n");
	scanf("%d", &s1.roll);
	Display(s1);

	return 0;
}

void Display(struct student stu){
	printf("Output:\nName: %s",stu.name);
	printf("\nRoll\:%d",stu.roll);
}
