/*
 ============================================================================
 Name        : StructuresinC.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Structures in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

struct student
{
	int id;
	char name[20];
	float percentage;
};

int main() {

	struct student record = {1,"Harry", 90.5};
	struct strudent *ptr;
	ptr = &record;


	printf("Id is: %d \n", ptr->id);
	printf("Name is %s \n", ptr->name);
	printf("Percentage is : %f \n", ptr->percentage);
	return 0;
}
