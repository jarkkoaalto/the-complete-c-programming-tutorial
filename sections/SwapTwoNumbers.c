/*
 ============================================================================
 Name        : SwapTwoNumbers.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Swap two numbers.c in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(){

	int a, b, c;

	printf("Enter a = \n");
	scanf("%d", &a);
	printf("Enter b = \n");
	scanf("%d", &b);

	c = a;
	a = b;
	b = c;
	printf("a = %d \n b = %d",a,b);


	return 0;

}
