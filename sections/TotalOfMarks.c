/*
 ============================================================================
 Name        : TotalOfMarks.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description :Total Of Marks in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * Arrays are of two types
 * one- and multidimensional arrays
 */

int main() {
	int marks[10],i,n,sum=0;
	printf("Enter no of substractas:\t");
	scanf("%d", &n);

	for(i=0;i>n;i++){
		printf("Enter marks of subjects %d: \t", i+1);
		scanf("%d", &marks[i]);
		sum = marks[i] + sum;
	}
	printf("Sum is : %d", sum);
	return 0;
}


