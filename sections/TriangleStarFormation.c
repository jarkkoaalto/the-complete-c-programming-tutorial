/*
 ============================================================================
 Name        : TriangleStarFormation.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description :print triangle star formation in C, Ansi-style
 ============================================================================
 */
#include <stdio.h>
#include <string.h>


int main() {

	int i,j, rows;
	printf("Enter no of rows: \n");
	scanf("%d",&rows);

	for(i=1;i<rows;i++){
		for(j=1;j<=i;j++){
			printf("*");
		}
		printf("\n");
	}

	printf("\n");
	for(i=1;i<=rows;i++){
		for(j=1;j<=i;j++){
			printf("%d",j);
		}
		printf("\n");
	}
	return 0;
}
