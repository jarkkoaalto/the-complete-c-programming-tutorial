/*
 ============================================================================
 Name        : Unions.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Unions in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

union job{
	char name[20];
	float salary;
	int worker_no;
}u;

struct job1{
	char name[32];
	float salary;
	int worker_no;
}s;

int main() {
		printf("Size of union = %d",sizeof(u));
		printf("\nSize of structure = %d",sizeof(s));
	return 0;
}


